#include "EntityQuerierSystem.h"

const TWeakPtr<FEntityQuery> & IEntityQuerierSystem::GetEntityQuery() const
{
	return _entityQuery;
}

const FEntityQuery & IEntityQuerierSystem::GetEntityQueryRef() const
{
	check(_entityQuery.IsValid());
	return *_entityQuery.Pin().Get();
}

void IEntityQuerierSystem::SetEntityQuery(const TWeakPtr<FEntityQuery> & entityQuery)
{
	_entityQuery = entityQuery;
}
