#include "Component.h"

IComponent::IComponent()
	: _entityId(0)
	, _siblings()
{
}

IComponent::~IComponent()
{
}

uint32 IComponent::GetEntityId() const
{
	return _entityId;
}

const TSet<IComponent *> & IComponent::GetSiblings() const
{
	return _siblings;
}

bool IComponent::IsType(const type_info & type) const
{
	return GetType() == type;
}
