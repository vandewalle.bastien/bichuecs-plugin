#include "EntityQuery.h"

using namespace Bichu::ECS;

FEntityQuery::FEntityQuery(const TWeakPtr<IEntityAdmin> & entityAdmin)
	: _entityAdmin(entityAdmin)
{
}

const IEntityAdmin & FEntityQuery::GetEntityAdmin() const
{
	check(_entityAdmin.IsValid());
	return *_entityAdmin.Pin();
}

TSet<FEntity> FEntityQuery::FindEntitiesWithComponents(const TArray<nullable_type_index> & allComponentTypes) const
{
	if (allComponentTypes.Num() == 0)
	{
		return TSet<FEntity>();
	}

	TSet<FEntity> tupleEntities = GetEntityAdmin().GetEntitiesByComponent(allComponentTypes[0].get_type_info());

	for (int32 tupleTypeIndex = 1; tupleTypeIndex < allComponentTypes.Num(); tupleTypeIndex++)
	{
		const nullable_type_index & tupleType = allComponentTypes[tupleTypeIndex];
		const TSet<FEntity> & entitiesByComponent = GetEntityAdmin().GetEntitiesByComponent(tupleType.get_type_info());

		TSet<FEntity> intersectedEntities;
		intersectedEntities.Reserve(entitiesByComponent.Num());

		for (const FEntity & entity : entitiesByComponent)
		{
			if (tupleEntities.Contains(entity))
			{
				intersectedEntities.Add(entity);
			}
		}

		tupleEntities = intersectedEntities;
	}

	return tupleEntities;
}
