#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Interface/EntityAdmin.h"
#include "Bichu/ECS/EntityFactory.h"

namespace Bichu
{
	namespace ECS
	{
		class FEntityFactory : public IEntityFactory
		{
		public:
			FEntityFactory(const TWeakPtr<IEntityAdmin> & entityAdmin);

		public:
			FEntityInitializer Build(uint32 EntityId, const TSet<IComponent *> Components) const final;
			void Destroy(uint32 EntityId) const final;

		private:
			TWeakPtr<IEntityAdmin> _entityAdmin;

		private:
			IEntityAdmin & GetEntityAdmin() const;

		private:
			void SetupComponents(uint32 EntityId, const TSet<IComponent *> Components) const;
		};
	}
}
