#include "EntityFactory.h"

// ECS
#include "Bichu/ECS/Entity/Entity.h"
#include "Bichu/ECS/Component.h"

using namespace Bichu::ECS;

FEntityFactory::FEntityFactory(const TWeakPtr<IEntityAdmin> & entityAdmin)
{
	_entityAdmin = entityAdmin;
}

IEntityAdmin & FEntityFactory::GetEntityAdmin() const
{
	check(_entityAdmin.IsValid());
	return *_entityAdmin.Pin();
}

FEntityInitializer FEntityFactory::Build(uint32 EntityId, const TSet<IComponent *> Components) const
{
	SetupComponents(EntityId, Components);

	const FEntity entity(EntityId, Components);

	GetEntityAdmin().CreateEntity(entity);

	return FEntityInitializer(EntityId, Components);
}

void FEntityFactory::SetupComponents(uint32 EntityId, const TSet<IComponent *> Components) const
{
	for (IComponent * component : Components)
	{
		component->_entityId = EntityId;
		component->_siblings = TSet<IComponent *>(Components.Array().FilterByPredicate([&](const IComponent * comp)
		{
			return component != comp;
		}));
	}
}

void FEntityFactory::Destroy(uint32 EntityId) const
{
	GetEntityAdmin().DestroyEntity(EntityId);
}
