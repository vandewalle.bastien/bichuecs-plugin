#include "SystemRootFactory.h"

#include "Bichu/ECS/System/SystemRoot.h"

TSharedPtr<ISystemRoot> FSystemRootFactory::Build(IScheduler * scheduler)
{
	return TSharedPtr<ISystemRoot>(new FSystemRoot(scheduler));
}
