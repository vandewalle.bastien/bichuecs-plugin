#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Interface/EntityAdmin.h"

#include "Bichu/ECS/Component/ComponentDataBase.h"
#include "Bichu/ECS/Component/ComponentRegistry.h"
#include "Bichu/ECS/Entity/EntityRegistry.h"
#include "Bichu/ECS/Entity/EntityStream.h"
#include "Bichu/ECS/Component.h"

namespace Bichu
{
	namespace ECS
	{
		class FEntityAdmin : public IEntityAdmin
		{
		public:
			FEntityAdmin(
				const TWeakPtr<FEntityStream> & EntityStream, 
				const TWeakPtr<FComponentDataBase> & ComponentDataBase);

		public:
			void CreateEntity(const FEntity & Entity) final;
			void DestroyEntity(uint32 EntityId) final;

			void SubmitEntitiesDestruction(const TArray<FEntity> & Entities) final;
			void SortComponentDataBase() final;

		public: // const
			IComponent * GetSingletonComponent(const type_info & Type) const final;

			FEntity GetEntity(uint32 EntityId) const final;
			const TArray<IComponent *> & GetComponents(const type_info & Type) const final;

			TArray<FEntity> GetEntities() const final;
			const TSet<FEntity> & GetEntitiesByComponent(const type_info & Type) const final;

		private:
			FEntityRegistry _entityRegistry;
			FComponentRegistry _componentRegistry;
			TWeakPtr<FComponentDataBase> _componentDataBase;
			TWeakPtr<FEntityStream> _entityStream;

		private:
			FEntityStream & GetEntityStream();

		private:
			void RegisterEntity(const FEntity & Entity);
			void UnregisterEntity(const FEntity & Entity);
			bool IsEntityRegistered(uint32 EntityId) const;
		};
	}
}