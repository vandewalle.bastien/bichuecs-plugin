#include "EntityAdmin.h"

#include "Bichu/ECS/Entity/Entity.h"

using namespace Bichu::ECS;

FEntityAdmin::FEntityAdmin(
	const TWeakPtr<FEntityStream> & EntityStream,
	const TWeakPtr<FComponentDataBase> & ComponentDataBase)
	: _entityRegistry()
	, _componentRegistry()
	, _componentDataBase(ComponentDataBase)
	, _entityStream(EntityStream)
{
}

FEntityStream & FEntityAdmin::GetEntityStream()
{
	check(_entityStream.IsValid());
	return *_entityStream.Pin();
}

void FEntityAdmin::CreateEntity(const FEntity & Entity)
{	
	if (!IsEntityRegistered(Entity.GetId()))
	{
		RegisterEntity(Entity);

		GetEntityStream().PushCreateEntity(Entity);
	}
}

bool FEntityAdmin::IsEntityRegistered(uint32 EntityId) const
{
	return _entityRegistry.HasEntity(EntityId);
}

void FEntityAdmin::DestroyEntity(uint32 id)
{
	FEntity entity = _entityRegistry.GetEntity(id);

	GetEntityStream().PushDestroyEntity(entity);
}

void FEntityAdmin::SubmitEntitiesDestruction(const TArray<FEntity> & Entities)
{
	for (const FEntity & entity : Entities)
	{
		UnregisterEntity(entity);

		_componentDataBase.Pin()->Destroy(entity.GetComponents());
	}
}

void FEntityAdmin::RegisterEntity(const FEntity & entity)
{
	_entityRegistry.RegisterEntity(entity);
	_componentRegistry.RegisterEntity(entity);
}

void FEntityAdmin::UnregisterEntity(const FEntity & entity)
{
	_entityRegistry.UnregisterEntity(entity);
	_componentRegistry.UnregisterEntity(entity);
}

void FEntityAdmin::SortComponentDataBase()
{
	const TMap<nullable_type_index, TArray<IComponent *>> dirtyComponentPools = _componentDataBase.Pin()->GatherDirtyComponentPools();

	for (const TPair<nullable_type_index, TArray<IComponent *>> & componentPool : dirtyComponentPools)
	{
		_componentRegistry.SetComponents(componentPool.Key, componentPool.Value);
	}
}

IComponent * FEntityAdmin::GetSingletonComponent(const type_info & Type) const
{
	const TArray<IComponent *> & allComponents = GetComponents(Type);
	check(allComponents.Num() == 1);
	return allComponents[0];
}

FEntity FEntityAdmin::GetEntity(uint32 EntityId) const
{
	return _entityRegistry.GetEntity(EntityId);
}

const TArray<IComponent *> & FEntityAdmin::GetComponents(const type_info & Type) const
{
	return _componentRegistry.GetComponents(Type);
}

const TSet<FEntity> & FEntityAdmin::GetEntitiesByComponent(const type_info & Type) const
{
	return _componentRegistry.GetEntitiesByComponent(Type);
}

TArray<FEntity> FEntityAdmin::GetEntities() const
{
	return _entityRegistry.GetEntities();
}
