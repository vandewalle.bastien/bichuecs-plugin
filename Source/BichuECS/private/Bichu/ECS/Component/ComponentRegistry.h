#pragma once

#include "CoreMinimal.h"
#include "Bichu/Common/Generic/nullabletype.h"
#include "Bichu/ECS/Entity/Entity.h"
#include "Bichu/ECS/Component.h"

namespace Bichu
{
	namespace ECS
	{
		class FComponentRegistry
		{
		public: // const
			const TArray<IComponent *> & GetComponents(const type_info & Type) const;
			const TSet<FEntity> & GetEntitiesByComponent(const type_info & Type) const;

		public:
			void RegisterEntity(const FEntity & Entity);
			void UnregisterEntity(const FEntity & Entity);

			void SetComponents(const nullable_type_index & Type, const TArray<IComponent *> & Components);

		private:
			static const TArray<IComponent *> NULL_COMPONENT_ARRAY;
			static const TSet<IComponent *> NULL_COMPONENT_SET;
			static const IComponent * NULL_COMPONENT_INFO;
			static const TSet<FEntity> NULL_ENTITIES_SET;

		private:
			TMap<nullable_type_index, TArray<IComponent *>> components;
			TMap<nullable_type_index, TSet<FEntity>> entities;

		private:
			void RegisterEntity(const type_info & Type, const FEntity & Entity);
			void RegisterComponent(IComponent * Component);
		
			void UnregisterEntity(const type_info & Type, const FEntity & Entity);
			void UnregisterComponent(IComponent * Component);
		};
	}
}