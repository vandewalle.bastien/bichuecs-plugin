#include "ComponentDataBase.h"

using namespace Bichu::ECS;

void FComponentDataBase::Destroy(const TMap<nullable_type_index, TSet<IComponent *>> & ComponentsByTypeIndex)
{
	for (const auto & componentsTypeIndexPair : ComponentsByTypeIndex)
	{
		Destroy(componentsTypeIndexPair.Key, componentsTypeIndexPair.Value);
	}
}

void FComponentDataBase::Destroy(const TSet<IComponent *> & Components)
{
	for (IComponent * component : Components)
	{
		Destroy(component);
	}
}

void FComponentDataBase::Destroy(IComponent * Component)
{
	FComponentPoolPtr * poolPtr = dataBase.Find(Component->GetType());

	if (poolPtr != nullptr)
	{
		poolPtr->Get()->Destroy(Component);
	}
}

void FComponentDataBase::Destroy(const nullable_type_index & TypeIndex, const TSet<IComponent *> & Components)
{
	FComponentPoolPtr * poolPtr = dataBase.Find(TypeIndex);

	if (poolPtr != nullptr)
	{
		poolPtr->Get()->Destroy(Components);
	}
}

TMap<nullable_type_index, TArray<IComponent *>> FComponentDataBase::GatherDirtyComponentPools()
{
	TMap<nullable_type_index, TArray<IComponent *>> dirtyComponentPools;

	for (const TPair<nullable_type_index, FComponentPoolPtr> & componentPool : dataBase)
	{
		if (componentPool.Value->IsDirty())
		{
			dirtyComponentPools.Add(componentPool.Key, componentPool.Value->GetAll());

			componentPool.Value->ClearDirtyFlag();
		}
	}

	return dirtyComponentPools;
}