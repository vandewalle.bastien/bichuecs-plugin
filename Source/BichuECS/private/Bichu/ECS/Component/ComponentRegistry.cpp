#include "ComponentRegistry.h"

#include "Bichu/ECS/Entity/Entity.h"

using namespace Bichu::ECS;

const TArray<IComponent *> FComponentRegistry::NULL_COMPONENT_ARRAY;
const TSet<IComponent *> FComponentRegistry::NULL_COMPONENT_SET;
const IComponent * FComponentRegistry::NULL_COMPONENT_INFO;
const TSet<FEntity> FComponentRegistry::NULL_ENTITIES_SET;

void FComponentRegistry::RegisterEntity(const FEntity & entity)
{
	for (IComponent * component : entity.GetComponents())
	{
		RegisterEntity(component->GetType(), entity);
		RegisterComponent(component);
	}
}

void FComponentRegistry::RegisterEntity(const type_info & Type, const FEntity & Entity)
{
	TSet<FEntity> * entitiesByComponent = entities.Find(Type);

	if (entitiesByComponent != nullptr)
	{
		entitiesByComponent->Add(Entity);
	}
	else
	{
		TSet<FEntity> newEntitiesByComponent;
		newEntitiesByComponent.Add(Entity);

		entities.Add(Type, newEntitiesByComponent);
	}
}

void FComponentRegistry::RegisterComponent(IComponent * Component)
{
	TArray<IComponent *> * pool = components.Find(Component->GetType());
		
	if (pool != nullptr)
	{
		pool->Add(Component);
	}
	else
	{
		TArray<IComponent *> newPool;
		newPool.Add(Component);

		components.Add(Component->GetType(), newPool);
	}
}

void FComponentRegistry::UnregisterEntity(const FEntity & entity)
{
	for (IComponent * component : entity.GetComponents())
	{
		UnregisterEntity(component->GetType(), entity);
		UnregisterComponent(component);
	}
}

void FComponentRegistry::UnregisterEntity(const type_info & Type, const FEntity & entity)
{
	TSet<FEntity> * entitiesByComponent = entities.Find(Type);
	check(entitiesByComponent != nullptr);

	entitiesByComponent->Remove(entity);
}

void FComponentRegistry::UnregisterComponent(IComponent * Component)
{
	TArray<IComponent *> * pool = components.Find(Component->GetType());
	check(pool != nullptr);

	pool->RemoveSwap(Component);
}

const TArray<IComponent *> & FComponentRegistry::GetComponents(const type_info & Type) const
{
	const TArray<IComponent *> * pool = components.Find(Type);

	if (pool != nullptr)
	{
		return *pool;
	}

	return NULL_COMPONENT_ARRAY;
}

const TSet<FEntity> & FComponentRegistry::GetEntitiesByComponent(const type_info & Type) const
{
	const TSet<FEntity> * entitiesByComponent = entities.Find(Type);

	if (entitiesByComponent != nullptr)
	{
		return *entitiesByComponent;
	}

	return NULL_ENTITIES_SET;
}

void FComponentRegistry::SetComponents(const nullable_type_index & Type, const TArray<IComponent *> & Components)
{
	check(components.Contains(Type));

	components[Type] = Components;
}
