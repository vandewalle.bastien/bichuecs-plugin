#include "SystemRoot.h"

#include "Bichu/ECS/EntityAdmin/EntityAdmin.h"
#include "Bichu/ECS/Entity/EntityStream.h"
#include "Bichu/ECS/Factory/EntityFactory.h"
#include "Bichu/ECS/ComponentFactory.h"
#include "Bichu/ECS/Scheduler.h"
#include "Bichu/ECS/System.h"
#include "Bichu/ECS/EntityQuerierSystem.h"
#include "Bichu/ECS/ComponentNotication.h"
#include "Bichu/ECS/System.h"

using namespace Bichu::ECS;

FSystemRoot::FSystemRoot(IScheduler * scheduler)
	: _entityStream(new FEntityStream())
	, _componentDataBase(new FComponentDataBase())
	, _entityAdmin(new FEntityAdmin(_entityStream, _componentDataBase))

	, _entityQuery(new FEntityQuery(_entityAdmin))

	, _systems()
	, _notifiedSystems()
{
	if (ensure(scheduler != nullptr))
	{
		scheduler->OnTick.AddRaw(this, &FSystemRoot::Tick);
	}
}

TSharedPtr<IEntityFactory> FSystemRoot::CreateEntityFactory() const
{
	return TSharedPtr<IEntityFactory>(new FEntityFactory(_entityAdmin));
}

TSharedPtr<FComponentFactory> FSystemRoot::CreateComponentFactory() const
{
	return MakeShared<FComponentFactory>(_componentDataBase);
}

void FSystemRoot::AddSystem(const TSharedPtr<ISystem> & system)
{
	check(system.IsValid())

	_systems.Add(system);

	IEntityQuerierSystem * queryingEntitySystem = dynamic_cast<IEntityQuerierSystem *>(system.Get());

	if (queryingEntitySystem != nullptr)
	{
		queryingEntitySystem->SetEntityQuery(_entityQuery);
	}

	IComponentNotication * componentNotification = dynamic_cast<IComponentNotication *>(system.Get());

	if (componentNotification != nullptr)
	{
		_notifiedSystems.Add(componentNotification);
	}

	system->Ready();
}

void FSystemRoot::Tick(float DeltaTime)
{
	TickSystems(DeltaTime);

	SubmitEntities();

	_entityAdmin->SortComponentDataBase();
}

void FSystemRoot::TickSystems(float deltaSeconds)
{
	for (const TSharedPtr<ISystem> & system : _systems)
	{
		system->Tick(deltaSeconds);
	}
}

void FSystemRoot::SubmitEntities()
{
	const TArray<FEntity> allCreatedEntities = _entityStream->GatherBeingCreatedEntities();
	const TArray<FEntity> allDestroyedEntities = _entityStream->GatherBeingDestroyedEntities();

	_entityStream->Clear();

	NotifyEntitiesCreation(allCreatedEntities);

	NotifyEntitiesDestruction(allDestroyedEntities);

	_entityAdmin->SubmitEntitiesDestruction(allDestroyedEntities);
}

void FSystemRoot::NotifyEntitiesDestruction(const TArray<FEntity> & allDestroyedEntities)
{
	for (const FEntity & destroyedEntity : allDestroyedEntities)
	{
		for (IComponentNotication * notifiedSystem : _notifiedSystems)
		{
			for (IComponent * component : destroyedEntity.GetComponents())
			{
				notifiedSystem->NotifyComponentDestroyed(component);
			}
		}
	}
}

void FSystemRoot::NotifyEntitiesCreation(const TArray<FEntity> & allCreatedEntities)
{
	for (const FEntity & destroyedEntity : allCreatedEntities)
	{
		for (IComponentNotication * notifiedSystem : _notifiedSystems)
		{
			for (IComponent * component : destroyedEntity.GetComponents())
			{
				notifiedSystem->NotifyComponentCreated(component);
			}
		}
	}
}
