#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/SystemRoot.h"

namespace Bichu
{
	namespace ECS
	{
		class IEntityAdmin;
		class FEntity;
		class FEntityStream;
		class FComponentDataBase;
	}
}

class FEntityQuery;
class IEntityFactory;
class FComponentFactory;
class IScheduler;
class ISystem;
class IComponentNotication;

class BICHUECS_API FSystemRoot final : public ISystemRoot
{
public:
	FSystemRoot(IScheduler * scheduler);

public: // ISystemRoot (const)
	TSharedPtr<IEntityFactory> CreateEntityFactory() const override final;
	TSharedPtr<FComponentFactory> CreateComponentFactory() const override final;

public: // ISystemRoot
	void AddSystem(const TSharedPtr<ISystem> & system) override final;

private:
	const TSharedRef<Bichu::ECS::FEntityStream> _entityStream;
	const TSharedRef<Bichu::ECS::FComponentDataBase> _componentDataBase;
	const TSharedRef<Bichu::ECS::IEntityAdmin> _entityAdmin;

	const TSharedRef<FEntityQuery> _entityQuery;

	TArray<TSharedPtr<ISystem>> _systems;
	TArray<IComponentNotication *> _notifiedSystems;

private:
	void Tick(float DeltaTime);
	void TickSystems(float deltaSeconds);

	void SubmitEntities();

	void NotifyEntitiesDestruction(const TArray<Bichu::ECS::FEntity> & allDestroyedEntities);
	void NotifyEntitiesCreation(const TArray<Bichu::ECS::FEntity> & allCreatedEntities);
};
