#include "Entity.h"

using namespace Bichu::ECS;

FEntity::FEntity()
	: Id(0)
	, Components()
{
}

FEntity::FEntity(const uint32 id, const TSet<IComponent *> & components)
	: Id(id)
	, Components(components)
{
}

uint32 FEntity::GetId() const
{
	return Id;
}

IComponent * FEntity::GetComponent(const type_info & Type) const
{
	for (IComponent * component : Components)
	{
		if (component->IsType(Type))
		{
			return component;
		}
	}

	return nullptr;
}

const TSet<IComponent *> & FEntity::GetComponents() const
{
	return Components;
}

bool FEntity::IsEquals(const FEntity& s) const
{
	return Id == s.Id;
}

bool FEntity::operator==(const FEntity& s) const
{
	return IsEquals(s);
}

bool FEntity::operator!=(const FEntity& s) const
{
	return !IsEquals(s);
}
