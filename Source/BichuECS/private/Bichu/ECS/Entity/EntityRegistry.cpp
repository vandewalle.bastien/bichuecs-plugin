#include "EntityRegistry.h"

using namespace Bichu::ECS;

void FEntityRegistry::RegisterEntity(const FEntity & entity)
{
	check(!_entities.Contains(entity.GetId()));

	_entities.Add(entity.GetId(), entity);
}

void FEntityRegistry::UnregisterEntity(const FEntity & entity)
{
	check(_entities.Contains(entity.GetId()));

	_entities.Remove(entity.GetId());
}

bool FEntityRegistry::HasEntity(uint32 entityId) const
{
	return _entities.Contains(entityId);
}

FEntity FEntityRegistry::GetEntity(uint32 entityId) const
{
	check(HasEntity(entityId));
	return _entities[entityId];
}

TArray<FEntity> FEntityRegistry::GetEntities() const
{
	TArray<FEntity> allEntities;
	_entities.GenerateValueArray(allEntities);
	return allEntities;
}