#include "EntityStream.h"

using namespace Bichu::ECS;

void FEntityStream::PushCreateEntity(const FEntity & entity)
{
	check(!IsBeingCreatedOrDestroyed(entity));

	_entityOperations.Add(FEntityOperation(entity, EEntitySubmitOperation::Create));
}

bool FEntityStream::IsBeingCreatedOrDestroyed(const FEntity & Entity) const
{
	return _entityOperations.ContainsByPredicate([&Entity](const FEntityOperation & entityOperation)
	{
		return entityOperation.Entity == Entity;
	});
}

void FEntityStream::PushDestroyEntity(const FEntity & Entity)
{
	const FEntityOperation entityOperation(Entity, EEntitySubmitOperation::Destroy);

	if (_entityOperations.Contains(entityOperation))
	{
		return;
	}

	_entityOperations.Add(entityOperation);
}

TArray<FEntity> FEntityStream::GatherBeingCreatedEntities()
{
	TArray<FEntity> beingCreatedEntities;

	for (const FEntityOperation & entityOperation : GetEntityOperations())
	{
		if (entityOperation.IsBeingCreated())
		{
			beingCreatedEntities.Add(entityOperation.Entity);
		}
	}

	return beingCreatedEntities;
}

TArray<FEntity> FEntityStream::GatherBeingDestroyedEntities()
{
	TArray<FEntity> beingDestroyedEntities;

	for (const FEntityOperation & entityOperation : GetEntityOperations())
	{
		if (entityOperation.IsBeingDestroyed())
		{
			beingDestroyedEntities.Add(entityOperation.Entity);
		}
	}

	return beingDestroyedEntities;
}

const TArray<FEntityOperation> & FEntityStream::GetEntityOperations() const
{
	return _entityOperations;
}

void FEntityStream::Clear()
{
	_entityOperations.Empty();
}
