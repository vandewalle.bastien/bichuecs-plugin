#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Entity/Entity.h"

namespace Bichu
{
	namespace ECS
	{
		class FEntityRegistry
		{
		public:
			void RegisterEntity(const FEntity & entity);
			void UnregisterEntity(const FEntity & entity);

			bool HasEntity(uint32 entityId) const;
			FEntity GetEntity(uint32 entityId) const;
			TArray<FEntity> GetEntities() const;

		private:
			TMap<uint32, FEntity> _entities;
		};
	}
}