#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Entity/Entity.h"

namespace Bichu
{
	namespace ECS
	{
		enum EEntitySubmitOperation
		{
			Create,
			Destroy
		};

		struct FEntityOperation
		{
		public:
			FEntityOperation()
				: Entity()
				, Operation(EEntitySubmitOperation::Create)
			{
			}

			FEntityOperation(const FEntity & _entity, EEntitySubmitOperation _operation)
				: Entity(_entity)
				, Operation(_operation)
			{
			}

		public:
			FEntity Entity;
			EEntitySubmitOperation Operation;

		public:
			bool IsBeingCreated() const { return Operation == EEntitySubmitOperation::Create; }
			bool IsBeingDestroyed() const { return Operation == EEntitySubmitOperation::Destroy; }

		public:
			bool operator==(const FEntityOperation& s) const
			{
				return Entity == s.Entity &&
					Operation == s.Operation;
			}
		};
	}
}
