#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Entity/EntityOperation.h"

namespace Bichu
{
	namespace ECS
	{
		class FEntityStream
		{
		public:
			void PushCreateEntity(const FEntity & entity);
			void PushDestroyEntity(const FEntity & entity);

			TArray<FEntity> GatherBeingCreatedEntities();
			TArray<FEntity> GatherBeingDestroyedEntities();

			void Clear();

		private:
			TArray<FEntityOperation> _entityOperations;

		private:
			const TArray<FEntityOperation> & GetEntityOperations() const;
			bool IsBeingCreatedOrDestroyed(const FEntity & Entity) const;
		};
	}
}