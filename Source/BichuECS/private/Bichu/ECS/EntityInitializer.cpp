#include "EntityInitializer.h"

FEntityInitializer::FEntityInitializer(uint32 inEntityId, TSet<IComponent *> inComponents)
	: EntityId(inEntityId)
	, Components(inComponents)
{
}

uint32 FEntityInitializer::GetEntityId() const
{
	return EntityId;
}

IComponent * FEntityInitializer::GetComponent(const type_info & Type) const
{
	for (IComponent * component : Components)
	{
		if (component->IsType(Type))
		{
			return component;
		}
	}

	return nullptr;
}
