#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class BICHUECS_API IComponentNotication
{
public:
	virtual ~IComponentNotication() {}

public:
	virtual void NotifyComponentCreated(IComponent * Component) const {}
	virtual void NotifyComponentDestroyed(IComponent * Component) const {}
};
