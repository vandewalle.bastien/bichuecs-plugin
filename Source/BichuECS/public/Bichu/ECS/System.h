#pragma once

#include "CoreMinimal.h"

class BICHUECS_API ISystem
{
public:
	virtual ~ISystem();

public:
	virtual void Ready() const {}
	virtual void Tick(float deltaSeconds) const {}
};
