#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/System.h"
#include "Bichu/ECS/EntityQuery.h"

class BICHUECS_API IEntityQuerierSystem : public ISystem
{
public:
	virtual void Ready() const override {}
	virtual void Tick(float deltaSeconds) const override {}

public:
	const TWeakPtr<FEntityQuery> & GetEntityQuery() const;
	const FEntityQuery & GetEntityQueryRef() const;

public:
	void SetEntityQuery(const TWeakPtr<FEntityQuery> & entityQuery);

private:
	TWeakPtr<FEntityQuery> _entityQuery;
};