#pragma once

#include "CoreMinimal.h"

class ISystemRoot;
class IScheduler;

class BICHUECS_API FSystemRootFactory
{
public:
	static TSharedPtr<ISystemRoot> Build(IScheduler * scheduler);
};