#pragma once

#include "CoreMinimal.h"
#include <typeinfo>

namespace Bichu { namespace ECS { class FEntityFactory; } }

class BICHUECS_API IComponent
{
public:
	IComponent();
	virtual ~IComponent() = 0;

public:
	virtual const type_info & GetType() const = 0;

public:
	uint32 GetEntityId() const;

	bool IsType(const type_info & type) const;

	const TSet<IComponent *> & GetSiblings() const;

public:
	template<class T = IComponent>
	bool IsType() const
	{
		return IsType(typeid(T));
	}

	template<class T = IComponent>
	T * GetSibling() const
	{
		for (IComponent * component : siblings)
		{
			if (component->IsType<T>())
			{
				return static_cast<T *>(component);
			}
		}

		return nullptr;
	}

	template<class T = IComponent>
	bool HasSibling() const
	{
		return GetSibling<T>() != nullptr;
	}

private:
	friend class Bichu::ECS::FEntityFactory;
	
	uint32 _entityId;
	TSet<IComponent *> _siblings;
};
