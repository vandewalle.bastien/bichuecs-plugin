#pragma once

#include "CoreMinimal.h"
#include "Bichu/Common/Generic/nullabletype.h"
#include "Bichu/ECS/Entity/Entity.h"
#include "Bichu/ECS/Interface/EntityAdmin.h"
#include "Bichu/ECS/Component.h"

template<class... Args>
using TComponentTuple = TTuple<Args *...>;

class BICHUECS_API FEntityQuery
{
public:
	template<class T = IComponent>
	FORCEINLINE T * GetSingletonComponent() const
	{
		return static_cast<T *>(GetEntityAdmin().GetSingletonComponent(typeid(T)));
	}

	template<class T = IComponent>
	FORCEINLINE T * GetComponent(uint32 EntityId) const
	{
		const Bichu::ECS::FEntity entity = GetEntityAdmin().GetEntity(EntityId);
		return entity.GetComponent<T>();
	}

	template<class T = IComponent>
	FORCEINLINE TArray<T *> ComponentItr() const
	{
		const TArray<IComponent *> & allComponentsSrc = GetEntityAdmin().GetComponents(typeid(T));

		TArray<T *> allComponents;
		allComponents.Reserve(allComponentsSrc.Num());

		for (IComponent * component : allComponentsSrc)
		{
			allComponents.Add(static_cast<T *>(component));
		}

		return allComponents;
	}

	template<class... Args>
	FORCEINLINE TComponentTuple<Args...> GetTuple(uint32 EntityId) const
	{
		const Bichu::ECS::FEntity entity = GetEntityAdmin().GetEntity(EntityId);
		return CreateTuple<Args...>(entity);
	}

	template<class... Args>
	FORCEINLINE TArray<TComponentTuple<Args...>> TupleItr() const
	{
		const TArray<nullable_type_index> tupleTypes = GetTupleTypes<Args...>();
		const TSet<Bichu::ECS::FEntity> tupleEntities = FindEntitiesWithComponents(tupleTypes);
		return CreateAllTuples<Args...>(tupleEntities);
	}

public:
	FEntityQuery(const TWeakPtr<Bichu::ECS::IEntityAdmin> & entityAdmin);

private:
	TWeakPtr<Bichu::ECS::IEntityAdmin> _entityAdmin;

private:
	const Bichu::ECS::IEntityAdmin & GetEntityAdmin() const;

private:
	template<class... Args>
	FORCEINLINE TArray<nullable_type_index> GetTupleTypes() const
	{
		return { typeid(Args)... };
	}

	TSet<Bichu::ECS::FEntity> FindEntitiesWithComponents(const TArray<nullable_type_index> & allComponentTypes) const;

	template<class... Args>
	FORCEINLINE TArray<TComponentTuple<Args...>> CreateAllTuples(const TSet<Bichu::ECS::FEntity> & tupleEntities) const
	{
		TArray<TComponentTuple<Args...>> allTuples;
		allTuples.Reserve(tupleEntities.Num());

		for (const Bichu::ECS::FEntity & entity : tupleEntities)
		{
			allTuples.Add(CreateTuple<Args...>(entity));
		}

		return allTuples;
	}

	template<class... Args>
	FORCEINLINE TComponentTuple<Args...> CreateTuple(const Bichu::ECS::FEntity & entity) const
	{
		return TComponentTuple<Args...>(GetComponent<Args>(entity)...);
	}

	template<class T = IComponent>
	FORCEINLINE T * GetComponent(const Bichu::ECS::FEntity & entity) const
	{
		return entity.GetComponent<T>();
	}
};
