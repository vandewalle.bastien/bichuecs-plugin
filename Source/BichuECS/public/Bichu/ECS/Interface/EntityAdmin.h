#pragma once

#include "CoreMinimal.h"

class IComponent;

namespace Bichu
{
	namespace ECS
	{
		class FEntity;

		class IEntityAdmin
		{
		public:
			virtual ~IEntityAdmin() {}

		public: // const
			virtual IComponent * GetSingletonComponent(const type_info & Type) const = 0;

			virtual const TArray<IComponent *> & GetComponents(const type_info & Type) const = 0;

			virtual FEntity GetEntity(uint32 EntityId) const = 0;
			virtual TArray<FEntity> GetEntities() const = 0;
			virtual const TSet<FEntity> & GetEntitiesByComponent(const type_info & Type) const = 0;

		public:
			virtual void CreateEntity(const FEntity & Entity) = 0;
			virtual void DestroyEntity(uint32 EntityId) = 0;

			virtual void SubmitEntitiesDestruction(const TArray<FEntity> & Entities) = 0;
			virtual void SortComponentDataBase() = 0;
		};
	}
}