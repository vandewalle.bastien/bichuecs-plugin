#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/EntityInitializer.h"

class IComponent;

class BICHUECS_API IEntityFactory
{
public:
	virtual ~IEntityFactory() {}

public:
	virtual FEntityInitializer Build(uint32 EntityId, const TSet<IComponent *> Components) const = 0;
	virtual void Destroy(uint32 EntityId) const = 0;
};
