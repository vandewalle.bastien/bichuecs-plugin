#pragma once

#include "CoreMinimal.h"

class IEntityFactory;
class FComponentFactory;
class ISystem;

class ISystemRoot
{
public:
	virtual ~ISystemRoot() {}

public:
	template<class T = ISystem>
	void AddSystem(T * system)
	{
		AddSystem(TSharedPtr<T>(system));
	}

public:
	virtual TSharedPtr<IEntityFactory> CreateEntityFactory() const = 0;
	virtual TSharedPtr<FComponentFactory> CreateComponentFactory() const = 0;

public:
	virtual void AddSystem(const TSharedPtr<ISystem> & system) = 0;
};
