#pragma once

#include "CoreMinimal.h"

class IComponent;

namespace Bichu
{
	namespace ECS
	{
		class BICHUECS_API FEntity
		{
		public:
			FEntity();
			FEntity(const uint32 id, const TSet<IComponent *> & components);

		public:
			uint32 GetId() const;

			const TSet<IComponent *> & GetComponents() const;

			template<class T = IComponent>
			T * GetComponent() const
			{
				return static_cast<T *>(GetComponent(typeid(T)));
			}

			IComponent * GetComponent(const type_info & Type) const;

		public:
			bool IsEquals(const FEntity& s) const;

			bool operator==(const FEntity& s) const;
			bool operator!=(const FEntity& s) const;

			friend FORCEINLINE uint32 GetTypeHash(const FEntity& s)
			{
				return s.Id;
			}

		private:
			uint32 Id;
			TSet<IComponent *> Components;
		};
	}
}
