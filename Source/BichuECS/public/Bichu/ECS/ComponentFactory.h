#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component/ComponentDataBase.h"

class BICHUECS_API FComponentFactory
{
public:
	FComponentFactory(const TWeakPtr<Bichu::ECS::FComponentDataBase> & inDataBase);

public:
	template<class T = IComponent>
	T * CreateComponent()
	{
		return dataBase.Pin()->CreateComponent<T>();
	}

private:
	TWeakPtr<Bichu::ECS::FComponentDataBase> dataBase;
};