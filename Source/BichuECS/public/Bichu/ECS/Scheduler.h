#pragma once

#include "CoreMinimal.h"

class IScheduler
{
public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnTick, float);
	FOnTick OnTick;
};