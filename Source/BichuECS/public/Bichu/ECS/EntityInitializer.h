#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

class BICHUECS_API FEntityInitializer
{
public:
	FEntityInitializer(uint32 inEntityId, TSet<IComponent *> inComponents = TSet<IComponent *>());

public:
	uint32 GetEntityId() const;

	IComponent * GetComponent(const type_info & Type) const;

	template<class T = IComponent>
	T * GetComponent() const
	{
		return static_cast<T *>(GetComponent(typeid(T)));
	}

	template<class T = IComponent>
	void Init(const T & Initializer)
	{
		T * component = GetComponent<T>();

		if (component != nullptr)
		{
			T cache = *component;

			*component = Initializer;
			component->IComponent::operator=(cache);
		}
	}

protected:
	uint32 EntityId;
	TSet<IComponent *> Components;
};
