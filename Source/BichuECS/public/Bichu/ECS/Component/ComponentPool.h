#pragma once

#include "CoreMinimal.h"
#include "Bichu/ECS/Component.h"

#define COMPONENT_POOL_SIZE 512

namespace Bichu
{
	namespace ECS
	{
		class IComponentPool
		{
		public:
			virtual ~IComponentPool() {}

		public:
			virtual	IComponent * Create() = 0;
			virtual void Destroy(const TSet<IComponent *> & Components) = 0;
			virtual void Destroy(IComponent * Component) = 0;

			virtual TArray<IComponent *> GetAll() = 0;

			virtual bool IsDirty() const = 0;
			virtual void ClearDirtyFlag() = 0;
		};

		template<class T = IComponent>
		class TComponentPool : public IComponentPool
		{
		public:
			TComponentPool()
				: pool()
				, size(0)
				, emptyIndices()
				, isDirty(false)
			{
				pool.SetNum(COMPONENT_POOL_SIZE, false);
			}

			T * Create() final
			{
				const int32 emptyIndex = GetFirstEmptyIndex();

				if (emptyIndex != INDEX_NONE)
				{
					isDirty = true;
					emptyIndices.Remove(emptyIndex);
					return &pool[emptyIndex];
				}

				check(size < COMPONENT_POOL_SIZE);
				return &pool[size++];
			}

			void Destroy(const TSet<IComponent *> & Components) final
			{
				for (IComponent * component : Components)
				{
					Destroy(component);
				}
			}

			void Destroy(IComponent * Component) final
			{
				check(Component->IsType<T>());

				const uint32 entityId = Component->GetEntityId();
				const int32 componentIndex = pool.IndexOfByPredicate([&entityId](const T & component)
				{
					return component.GetEntityId() == entityId;
				});

				check(componentIndex != INDEX_NONE);

				emptyIndices.Add(componentIndex);
				pool[componentIndex] = T();
			}

			TArray<IComponent *> GetAll() final
			{
				TArray<IComponent *> allComponents;
				allComponents.Reserve(size - emptyIndices.Num());

				for (int32 componentIndex = 0; componentIndex < size; componentIndex++)
				{
					if (!emptyIndices.Contains(componentIndex))
					{
						allComponents.Add(static_cast<IComponent *>(&pool[componentIndex]));
					}
				}

				return allComponents;
			}

			bool IsDirty() const final
			{
				return isDirty;
			}

			void ClearDirtyFlag() final
			{
				isDirty = false;
			}

		private:
			TArray<T, TFixedAllocator<COMPONENT_POOL_SIZE>> pool;
			int32 size;
			TSet<int32> emptyIndices;
			bool isDirty;

		private:
			int32 GetFirstEmptyIndex() const
			{
				for (const int32 emptyIndex : emptyIndices)
				{
					return emptyIndex;
				}

				return INDEX_NONE;
			}
		};

	}
}
