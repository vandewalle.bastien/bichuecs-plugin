#pragma once

#include "CoreMinimal.h"
#include "Bichu/Common/Generic/nullabletype.h"
#include "Bichu/ECS/Component/ComponentPool.h"
#include "Bichu/ECS/Component.h"

namespace Bichu
{
	namespace ECS
	{
		using FComponentPoolPtr = TSharedPtr<IComponentPool>;

		class FComponentDataBase
		{
		public:
			template<class T = IComponent>
			T * CreateComponent()
			{
				const type_info & type = typeid(T);
				FComponentPoolPtr * poolPtr = dataBase.Find(type);

				if (poolPtr != nullptr)
				{
					return static_cast<TComponentPool<T> *>(poolPtr->Get())->Create();
				}

				TComponentPool<T> * newPool = new TComponentPool<T>();
				dataBase.Add(type, FComponentPoolPtr(newPool));
				return newPool->Create();
			}

			void Destroy(const TMap<nullable_type_index, TSet<IComponent *>> & ComponentsByTypeIndex);
			void Destroy(const TSet<IComponent *> & Components);
			void Destroy(IComponent * Component);

			TMap<nullable_type_index, TArray<IComponent *>> GatherDirtyComponentPools();

		private:
			TMap<nullable_type_index, FComponentPoolPtr> dataBase;

		private:
			void Destroy(const nullable_type_index & TypeIndex, const TSet<IComponent *> & Components);
		};
	}
}
