using UnrealBuildTool;

public class BichuECS : ModuleRules
{
    public BichuECS(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bUseRTTI = true;
        bAddDefaultIncludePaths = true;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "BichuCommon" });

        PrivateDependencyModuleNames.AddRange(new string[] { });
    }
}
