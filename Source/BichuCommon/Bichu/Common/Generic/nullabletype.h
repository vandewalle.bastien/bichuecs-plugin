#pragma once

#include "CoreMinimal.h"
#include <typeindex>
#include <functional>
#include <unordered_map>
#include <cassert>

struct nullable_type_index
{
	constexpr nullable_type_index() : ptr_(nullptr) {}
	constexpr nullable_type_index(std::type_info const& ti) : ptr_(std::addressof(ti)) {}

	constexpr operator bool() const
	{
		return bool(ptr_);
	}

	// I have avoided implicit conversion, but it could probably work
	// without any surprises.
	std::type_info const& get_type_info() const {
		assert(ptr_);
		return *ptr_;
	}

	constexpr bool operator==(nullable_type_index const& other) const {
		return ptr_ && other.ptr_
			? *ptr_ == *other.ptr_
			: ptr_ == other.ptr_;
	}

	friend FORCEINLINE uint32 GetTypeHash(const nullable_type_index& s)
	{
		return s ? 0 : s.get_type_info().hash_code();
	}

private:
	std::type_info const* ptr_;
};
