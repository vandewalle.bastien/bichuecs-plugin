#pragma once

#include "CoreMinimal.h"
#include "GenericPlatform/GenericPlatformMisc.h"

template<typename T>
class TDispatchOnSet
{
	DECLARE_MULTICAST_DELEGATE_TwoParams(FOnSet, uint32, T);

public:
	TDispatchOnSet()
		: senderId(0)
		, value()
		, paused(false)
	{
	}

	TDispatchOnSet(uint32 inSenderId, T inValue)
		: TDispatchOnSet()
	{
		senderId = inSenderId;
		value = inValue;
	}

public:
	void SetValue(T inValue)
	{
		value = inValue;

		if (paused == false)
		{
			onSetBool.Broadcast(senderId, value);
		}
	}

	T GetValue() const
	{
		return value;
	}

	template<class UserClass>
	void NotifyOnValueSet(UserClass * Object, typename TMemFunPtrType<false, UserClass, void(uint32, T)>::Type Func)
	{
		onSetBool.AddRaw(Object, Func);
	}

	template<class UserClass>
	void NotifyOnValueSet(UserClass * Object, typename TMemFunPtrType<true, UserClass, void(uint32, T)>::Type Func)
	{
		onSetBool.AddRaw(Object, Func);
	}

	void StopNotify(FDelegateHandle Handle)
	{
		onSetBool.Remove(Handle);
	}

	void PauseNotify()
	{
		paused = true;
	}

	void ResumeNotify()
	{
		paused = false;
	}

private:
	uint32 senderId;
	T value;
	bool paused;
	FOnSet onSetBool;
};
