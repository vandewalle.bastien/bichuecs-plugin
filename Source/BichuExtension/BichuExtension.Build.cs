using UnrealBuildTool;

public class BichuExtension : ModuleRules
{
    public BichuExtension(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bUseRTTI = true;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "BichuECS", "BichuExtension" });

        PrivateDependencyModuleNames.AddRange(new string[] { });
    }
}
