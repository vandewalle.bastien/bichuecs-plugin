#pragma once

#include "CoreMinimal.h"
#include "Object.h"
#include "Bichu/Extension/EntityObjectFactory.h"
#include "UnrealEntityFactory.generated.h"

class IEntityFactory;

UCLASS()
class BICHUEXTENSION_API UUnrealEntityFactory : public UObject, public IEntityObjectFactory
{
	GENERATED_BODY()

	UUnrealEntityFactory();

public:
	void Setup(const TSharedPtr<IEntityFactory> & entityAdmin);

public:
	FEntityInitializer BuildEntityObject(UObject * Object, TSet<IComponent *> Components) final;
	FEntityInitializer BuildEntityObject(AActor * Actor, TSet<IComponent *> Components = TSet<IComponent *>()) final;

	UObject * DestroyEntityObject(uint32 EntityId) final;

	UObject * GetEntityObject(uint32 EntityId) final;

private:
	UPROPERTY()
	TMap<uint32, UObject *> _entityObjects;

	TSharedPtr<IEntityFactory> _entityFactory;

private:
	IEntityFactory & GetEntityFactory() const;

private:
	FEntityInitializer BuildEntity(uint32 EntityId, const TSet<IComponent *> & Component) const;

	void DestroyEntity(uint32 EntityId);
};
