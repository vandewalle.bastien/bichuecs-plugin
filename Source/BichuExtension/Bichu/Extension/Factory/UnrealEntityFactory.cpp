#include "UnrealEntityFactory.h"

// Engine
#include "GameFramework/Actor.h"

// ECS
#include "Bichu/ECS/Component.h"
#include "Bichu/ECS/EntityFactory.h"
#include "Bichu/Extension/Implementator.h"

UUnrealEntityFactory::UUnrealEntityFactory()
	: Super()
	, _entityObjects()
	, _entityFactory(nullptr)
{
}

void UUnrealEntityFactory::Setup(const TSharedPtr<IEntityFactory> & entityAdmin)
{
	_entityFactory = entityAdmin;
}

IEntityFactory & UUnrealEntityFactory::GetEntityFactory() const
{
	check(_entityFactory.IsValid());
	return *_entityFactory.Get();
}

#pragma region IEntityFactory

FEntityInitializer UUnrealEntityFactory::BuildEntityObject(AActor * Actor, TSet<IComponent *> Components)
{
	const uint32 entityId = Actor->GetUniqueID();

	for (UActorComponent * actorComponent : Actor->GetComponents())
	{
		if (actorComponent->GetClass()->ImplementsInterface(UImplementator::StaticClass()))
		{
			IComponent * Component = dynamic_cast<IComponent *>(actorComponent);

			if (Component != nullptr)
			{
				Components.Add(Component);
			}
		}
	}

	return BuildEntityObject(CastChecked<UObject>(Actor), Components);
}

FEntityInitializer UUnrealEntityFactory::BuildEntityObject(UObject * Object, TSet<IComponent *> Components)
{
	const uint32 entityId = Object->GetUniqueID();

	if (Components.Num() > 0)
	{
		_entityObjects.Add(entityId, Object);
		return BuildEntity(entityId, Components);
	}

	return FEntityInitializer(entityId);
}

#pragma endregion IEntityFactory

FEntityInitializer UUnrealEntityFactory::BuildEntity(uint32 EntityId, const TSet<IComponent *> & Components) const
{
	return GetEntityFactory().Build(EntityId, Components);
}

void UUnrealEntityFactory::DestroyEntity(const uint32 EntityId)
{
	GetEntityFactory().Destroy(EntityId);
	_entityObjects.Remove(EntityId);
}

UObject * UUnrealEntityFactory::DestroyEntityObject(uint32 EntityId)
{
	UObject ** entityObject = _entityObjects.Find(EntityId);

	if (entityObject)
	{
		UObject * object = *entityObject;

		DestroyEntity(EntityId);

		return object;
	}

	return nullptr;
}

UObject * UUnrealEntityFactory::GetEntityObject(uint32 EntityId)
{
	return _entityObjects.FindRef(EntityId);
}
