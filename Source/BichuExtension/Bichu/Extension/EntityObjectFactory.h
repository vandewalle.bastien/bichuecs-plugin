#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Bichu/ECS/EntityInitializer.h"
#include "EntityObjectFactory.generated.h"

class AActor;
class UObject;
class IComponent;

UINTERFACE()
class BICHUEXTENSION_API UEntityObjectFactory : public UInterface
{
	GENERATED_BODY()
};

class BICHUEXTENSION_API IEntityObjectFactory
{
	GENERATED_BODY()

public:
	virtual FEntityInitializer BuildEntityObject(UObject * Object, TSet<IComponent *> Components) = 0;
	virtual FEntityInitializer BuildEntityObject(AActor * Actor, TSet<IComponent *> Components = TSet<IComponent *>()) = 0;

	virtual UObject * GetEntityObject(uint32 EntityId) = 0;

	virtual UObject * DestroyEntityObject(uint32 EntityId) = 0;

public:
	template<class... Args, class UUserClass = UObject>
	FEntityInitializer BuildEntityObject(UUserClass * Object)
	{
		return BuildEntityObject(Object, { static_cast<Args *>(Object)... });
	}

	template<class UUserClass = UObject>
	UUserClass * GetEntityObject(uint32 EntityId)
	{
		return Cast<UUserClass>(GetEntityObject(EntityId));
	}
};
