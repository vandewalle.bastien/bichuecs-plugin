#include "UnrealScheduler.h"

AUnrealScheduler::AUnrealScheduler()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AUnrealScheduler::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (OnTick.IsBound())
	{
		OnTick.Broadcast(DeltaSeconds);
	}
}
