#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bichu/ECS/Scheduler.h"
#include "UnrealScheduler.generated.h"

UCLASS(Blueprintable)
class BICHUEXTENSION_API AUnrealScheduler : public AActor, public IScheduler
{
	GENERATED_BODY()

	AUnrealScheduler();

public:
	void Tick(float DeltaSeconds) override final;
};
