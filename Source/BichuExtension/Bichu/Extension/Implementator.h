#pragma once

#include "UObject/Interface.h"
#include "Implementator.generated.h"

UINTERFACE()
class BICHUEXTENSION_API UImplementator : public UInterface
{
	GENERATED_BODY()
};

class BICHUEXTENSION_API IImplementator
{
	GENERATED_BODY()
};